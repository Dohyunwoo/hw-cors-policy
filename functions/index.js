const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

exports.test = functions.https.onCall((data, context) => {
  const store = admin.firestore();
  store.collection('cities').add({ name: "Los Angeles", state: "CA", country: "USA"})
  .then(() => { return { success: true } })
  .catch((err) => { return { success: false, error: err } })
});
