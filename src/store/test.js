import Vue from 'vue'
import VueCompositionApi, { reactive } from '@vue/composition-api'

Vue.use(VueCompositionApi) 

const test = reactive({
  docs: [],
  createClickHandle: true,
  readClickHandle: true,
  fetchFuncReq() {
    test.createClickHandle = false
    const addDoc = firebase.functions().httpsCallable('test')
    addDoc().then(() => { 
      alert('Document 데이터 추가 성공')
    }).catch(err => {
      alert('Document 데이터 추가 실패')
      console.log(err)
    }).finally(() => {
      test.createClickHandle = true
    })
  },
  getDoc() {
    test.docs = []
    test.readClickHandle = false
    firebase.firestore().collection('cities').get().then(res => {
      if(res.empty) return false
      res.docs.filter(doc => { test.docs.push(doc.data()) })
      return test.docs
    }).catch(err => {
      alert('Document 데이터 불러오기 실패', err)
    }).finally(() => {
      test.readClickHandle = true
    })
  }
})

export default test
